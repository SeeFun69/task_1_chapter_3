create database final;

use final;
CREATE TABLE `customer` (
id_customer int not null auto_increment
primary key,
id_order int,
nama varchar(100),
alamat varchar(200)
  );
  alter table customer
  drop id_order;

create table product  
(	
	id_product int not null auto_increment,
	Nama_product VARCHAR (50),
	harga INT ,
	stok INT,
    primary key(id_product),
    no_supplier int,
    kategori int,
    constraint no_supplier foreign key(no_supplier) references supplier (id_supplier)
);

create table sales
(
ID_sales INT not null auto_increment
primary key,
nama_sales VARCHAR (20),
No_handphone VARCHAR (12)
);

create table supplier
(
ID_Supplier int not null auto_increment
primary key,
No_Handphone varchar(14),
Tanggal_Supplay date
);


CREATE TABLE tblOrder (
	No_Order INT not null auto_increment
    primary key,
	No_Customer INT not null,
	Nomor_Sales INT not null,
    No_Product INT not null,
     Jumlah_Product int not null,
     Total_Bayar int not null,
    constraint no_customer foreign key(no_customer) references customer (id_customer),
	constraint nomor_sales foreign key(nomor_sales) references sales (id_sales),
    constraint no_product foreign key(no_product) references product (id_product)
);

create table kategori(
id_kategori int not null auto_increment
primary key,
jenis_barang varchar (50),
Lokasi_barang varchar (50)
);

alter table product
add constraint fk_barang_kategori
foreign key(Kategori)
references Kategori(id_Kategori);

use final;
select * from tblorder inner join customer on customer.id_customer = tblorder.No_Customer;
select * from customer;
select * from product;
select * from supplier;
select * from product inner join tblorder on product.id_product = tblorder.No_Product;
select * from product inner join tblorder on product.id_product = tblorder.No_Product
inner join kategori on kategori.id_kategori = product.kategori ;